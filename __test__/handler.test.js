import {branch_name} from "../handler"

test("test branch name", () => {
    expect(branch_name()).toEqual({"branch":"master", "revision":2});
});